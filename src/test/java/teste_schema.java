import io.restassured.module.jsv.JsonSchemaValidator;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;


public class teste_schema {

    @Test
    public  void vaidarcerto(){

        when()
                .get("https://api.qa.bancobari.com.br/onboarding/v1/leads/incomerange")
                .then().statusCode(HttpStatus.SC_OK)
                .body(matchesJsonSchemaInClasspath("schema.json"));
    }

    @Test
    public void teste2() {
        given()
                .log().all()
                .when()
                    .get("https://api.qa.bancobari.com.br/onboarding/v1/leads/incomerange")
                .then()
                    .log().all()
                    .statusCode(200)
                    .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schema.json"));
    }
}
