import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.hamcrest.core.Is;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.testng.FileAssert.fail;


public class teste {

    @BeforeTest
    private static String getTokenUser() {
        try {
            Response client = given()
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .formParam("client_id", "onboarding")
                    .formParam("client_secret", "7sjJhLEyKIIz")
                    .formParam("grant_type", "client_credentials")
                    .post("https://api.qa.bancobari.com.br/auth/connect/token")
                    .then()
                    .statusCode(200)
                    .extract()
                    .response();

            return new JsonPath(client.body().asString()).get("access_token");
        } catch (Exception e) {
            e.printStackTrace();
            fail("Erro ao requisitar token authorization.");
            return "";
        }
    }

    @Test
            public void documents(){
                 when()
                .get("https://api.qa.bancobari.com.br/onboarding/v1/documents/063371629503")
                .then()
                .statusCode(200)
                .statusCode(Matchers.is(200))
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schemaDocuments.json"))
                .body(Matchers.is("{\"status\":104}"));
    }

//    @Test
//    public void livenessProcess(){
//        given()
//                .header("Content-Type","application/json-patch+json")
//                .body("{\n" +
//                        "  \"eventDate\": \"string\",\n" +
//                        "  \"event\": \"string\",\n" +
//                        "  \"data\": {\n" +
//                        "    \"id\": \"string\",\n" +
//                        "    \"status\": \"string\"\n" +
//                        "  }\n" +
//                        "}")
//
//        .when()
//        .post("https://api.qa.bancobari.com.br/onboarding/v1/endpoints/livenessprocess")
//                .then()
//                .log().all()
//                .body(matchesJsonSchemaInClasspath("schemaLiveness.json"));
//    }


    @Test
    public void leads() {

        given()
                .header("User-Agent", "6d7e36ec-b664-4fbc-b4bc-74ac09038dd8")
                .header("Authorization", "Bearer " + getTokenUser())
                .log().headers()
                .get("https://api.qa.bancobari.com.br/onboarding/v1/leads/23a96e9f-329a-43a3-aec4-4dcaf8bc5f19")
                .then()
                .statusCode(200)
                .body(matchesJsonSchemaInClasspath("schemaLeads.json"))
                .log().all();


    }

    @Test
    public void incomeRange() {
        when().
                get("https://api.qa.bancobari.com.br/onboarding/v1/leads/incomerange")
                .then()
                .statusCode(200)
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schema.json"))
                .body(Matchers.is("[{\"key\":\"RANGE1\",\"value\":\"Até R$ 2.500,00\"}," +
                        "{\"key\":\"RANGE2\",\"value\":\"De R$ 2.500,01 a R$ 5.000,00\"}" +
                        ",{\"key\":\"RANGE3\",\"value\":\"De R$ 5.000,01 a R$ 7.500,00\"}" +
                        ",{\"key\":\"RANGE4\",\"value\":\"De R$ 7.500,01 a R$ 10.000,00\"}" +
                        ",{\"key\":\"RANGE5\",\"value\":\"De R$ 10.000,01 a R$ 15.000,00\"}" +
                        ",{\"key\":\"RANGE6\",\"value\":\"Acima de R$ 15.000,00\"}]"));
    }


    //    @BeforeTest
//    public String tokenOnboarding() {
//
//        Response resp = RestAssured
//                .given()
//                .header("Content-Type", "application/x-www-form-urlencoded")
//                .formParam("client_id", "onboarding")
//                .formParam("client_secret", "7sjJhLEyKIIz")
//                .formParam("grant_type", "client_credentials")
//                .when()
//                .post("https://api.qa.bancobari.com.br/auth/connect/token")
//                .then()
//                .statusCode(200)
//                .extract().response();
//
//
//        String token = resp.jsonPath().get("acess_token");
//        System.out.println(resp.jsonPath().prettify());
//        System.out.println("response:"+ resp.asString());
//
//        return token;
//
//    }


}
